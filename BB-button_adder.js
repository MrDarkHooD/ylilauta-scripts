// ==UserScript==
// @name        BB-styling buttons adder - ylilauta.org
// @match       *://ylilauta.org/*
// @version     1.1
// @grant       none
// @author      MrDarkHooD
// ==/UserScript==

function addGlobalStyle(css) {
    const nonce = document.getElementsByTagName("script")[0].nonce;
    const head = document.getElementsByTagName('head')[0];
    const style = document.createElement('style');
    style.type = 'text/css';
    style.nonce = nonce;
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle('.BB-buttons button { font-size: 1.1em }')
addGlobalStyle('.BB-buttons .BB-b { font-weight: bold !important; }')
addGlobalStyle('.BB-buttons .BB-em { font-style: italic !important; }')
addGlobalStyle('.BB-buttons .BB-u { text-decoration: underline; !important; }')
addGlobalStyle('.BB-buttons .BB-s { text-decoration: line-through; !important; }')

addGlobalStyle('.BB-buttons .BB-spoiler { background: linear-gradient(to bottom right, var(--button-bg-color) 30%, var(--body-bg)) }')

addGlobalStyle('.BB-buttons .BB-code::before { content: "<"; font-style: italic; }')
addGlobalStyle('.BB-buttons .BB-code::after { content: ">"; font-style: italic; }')

addGlobalStyle('.BB-buttons .BB-quote { font-style: italic; }')
addGlobalStyle('.BB-buttons .BB-quote::before { content: "\„"; font-style: italic; }')
addGlobalStyle('.BB-buttons .BB-quote::after { content: "\“"; font-style: italic; }')

const options = ['b', 'em', 'u', 's', 'spoiler', 'code', 'quote']

const form = document.querySelector("#post-form")
const messageField = form.querySelector("textarea")

const buttonDiv = document.createElement("div");
buttonDiv.classList.add("BB-buttons");
messageField.parentElement.before(buttonDiv);

options.forEach(option => {
    const newButton = document.createElement("button");
    newButton.innerText = option;
    newButton.setAttribute("type", "button")
    newButton.classList.add("BB-" + option)
    newButton.setAttribute("data-action", "Textarea.BBcode")
    newButton.setAttribute("data-tag", option)
  
    buttonDiv.appendChild(newButton);
  
})

function clickHandler(e) {
    if (e.target.dataset.action !== 'Textarea.BBcode') {
      return //Let everything else work normally
    }
  
    e.stopPropagation(); //Prevent event to start original handler
    e.preventDefault();
  
    const tag = e.target.dataset.tag

    const form = document.querySelector("#post-form")
    const messageField = form.querySelector("textarea")
  
    let startPos = messageField.selectionStart
    let endPos = messageField.selectionEnd
    
          messageField.value = messageField.value.substring(0, startPos)
            + "[" + tag + "]"
            + messageField.value.slice(startPos, endPos)
            + "[/" + tag + "]"
            + messageField.value.substring(endPos, messageField.value.length)
      
        messageField.focus()
        messageField.selectionEnd = startPos + tag.length + 2
}

document.querySelector('#post-form').addEventListener('click', clickHandler);
