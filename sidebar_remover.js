// ==UserScript==
// @name     Remove sidebar - ylilauta.org
// @match    *://ylilauta.org/*
// @version  1.4
// @grant    none
// @author   MrDarkHooD
// ==/UserScript==

function addGlobalStyle(css) {
	const nonce = document.getElementsByTagName("script")[0].nonce;
	const head = document.getElementsByTagName('head')[0];
	const style = document.createElement('style');
	style.type = 'text/css';
	style.nonce = nonce;
	style.innerHTML = css;
	head.appendChild(style);
}

addGlobalStyle('body { padding-top: 5px !important; padding-bottom: 5px !important; }');
addGlobalStyle('#sidebar, #navbar { display: none; }');
addGlobalStyle('#content { margin: 0 5px; padding-left: 0px  }');
addGlobalStyle('#topBar { margin: 0 15px; }');
addGlobalStyle('.red.bubble { position: relative; }');
addGlobalStyle('.pointer {cursor: pointer;}');

const shortNames = {
    satunnainen: 'b',
    palaute: 'meta',
    pub: 'rr',
    matkailu: 'coco',
    huumeet: 'h',
    ohjelmointi: 'o',
    masiinat: 'm',
    musiikki: 'mu',
    talous: 'biz',
    japanijutut: 'a',
    perhe: 'arki',
    homoilu: 'fag',
    rule34: 'rule34',
    turri: 'turri',
    kuntosali: 'fit',
    hisparefugio: 'spa',
    usofono: 'pt',
    scandinavia: 'scan',
    russia: 'ru',
    naistenhuone: 'nh',
    sota: 'sota',
    pelit: 'g',
    muoti: 'fa',
    ihmissuhteet: 'soc',
    deitti: 'd',
    bigbrother: 'bb',
    kasityot: 'fap',
    international: 'int',
    taide: 'art',
    televisio: 'tv',
    matkustus: 'coco',
    paranormaali: 'x',
    juorut: 'j',
    videot: 'mp4',
    rikokset: 'r',
    tubetus: 'yt',
    uutiset: 'u',
    suhteet: 'sopo',
    opiskelu: 'sch',
    terveys: 'dr',
    kirjallisuus: 'lit',
    arkisto: 'epic',
    luonto: 'lu',
    nikotiinipussit: 'np',
    penkkiurheilu: 'pu',
    tekoaly: 'ai',
    ruokajajuoma: 'nom',
    uhkapelit: 'win',
    aihevapaa: 'av',
    akuankka: 'aku',
    kryptovaluutat: 'kv',
    ajoneuvot: 'p',
    yhteiskunta: 'pol',
    joulukalenteri: 'jk',
    bilderberg: 'roll',
    tiede: 'sci',
    ninja: 'n',
    koronavirus: 'kv',
    homoseksuaalisuus: 'fag',
    seksuaalisuus: 'sex',
    ww3: 'ww3',
    tori: 'tori',
    koti: 'koti',
    mielenterveys: 'mt',
    jorma: 'erä',
    harrastukset: 'sp',
    diy: 'diy',
    casinofoorumi: 'cf',
    meemivala: 'mv',
    mobiili: 'mobile',
    laki: 'law',
    tyo: 'työ',
    hikky: 'hikky',
    uskonnot: 'usk',
    lusofono: 'por',
    ansat: 'trap',
    elektroniikka: 'el',
    esports: 'esp',
    de: 'de',
    eesti: 'ee'
}


const contentDiv = document.querySelector('#content');
const sideBar = document.getElementById("sidebar");

const tabs = ["#nav-boards", "[data-tab-id='profile']", "#nav-threads"];

const ogNotif = document.querySelector("[data-notifications-fn]").querySelector("span")
const ogFollow = document.querySelector("[data-thread-follow-box-fn]").querySelector("span")

let notiCount = ogNotif ? ogNotif.innerText : '0';
let FollCount = ogFollow ? ogFollow.innerText : '0';

const followed = document.createElement('a');
followed.dataset.threadFollowBoxFn = 'open'
followed.classList.add('pointer')
followed.innerHTML = ' [ Followed ' + FollCount + ' ] '

const notifications = document.createElement('a');
notifications.dataset.notificationsFn = 'open'
notifications.classList.add('pointer')
notifications.innerHTML = ' [ Notifications ' + notiCount + ' ] '


const topBar = document.createElement('div');
topBar.setAttribute("id", "topBar");
contentDiv.prepend(topBar);

document.querySelector("[data-notifications-fn]").remove()
document.querySelector("[data-thread-follow-box-fn]").remove()

tabs.forEach((tab) => {
	const currentTab = sideBar.querySelector(tab);

  if (currentTab.dataset.tabId == 'profile') {
	  		topBar.appendChild(notifications);
	  		topBar.appendChild(followed);
			}
  
	currentTab.querySelectorAll("nav a").forEach((link) => {

		// Make board names shorter
		if (currentTab.getAttribute("id") == "nav-boards") {
			let boardUrlName = link.getAttribute("href").replace(/\//g, "");
			if (shortNames[boardUrlName]) {
				link.innerHTML = shortNames[boardUrlName];
				link.classList.add("laudat");
			} else {
				link.innerHTML = boardUrlName;
			}
		}

    console.log(currentTab.classList)
    
		if ( !link.classList.contains('level') )
    {
	  
			topBar.append(link);
			const slash = document.createElement('span');
			slash.textContent = " / ";
			topBar.appendChild(slash);
    }
	});

	if (currentTab.dataset.tabId == 'profile') {
		topBar.innerHTML += "<br />";
	}

});

contentDiv.append(topBar.cloneNode(true));