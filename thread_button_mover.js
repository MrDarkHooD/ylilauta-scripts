// ==UserScript==
// @name         Action button mover - ylilauta.org
// @description  Moves action buttons to original place without hamburger menu. Also adds reverse imagesearch button.
// @match        *://ylilauta.org/*
// @version      1.8
// @grant        none
// @author       MrDarkHooD
// ==/UserScript==

function addGlobalStyle(css) {
    const nonce = document.getElementsByTagName("script")[0].nonce;
    const head = document.getElementsByTagName('head')[0];
    const style = document.createElement('style');
    style.type = 'text/css';
    style.nonce = nonce;
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle('.style-new_replies .thread-meta .functions { display: none !important; }')
addGlobalStyle('.icon-bookmark2.active { color: var(--body-bg) !important; }')

addGlobalStyle('.threads.style-grid .thread .thread-buttons { position: unset; display: block; }')
addGlobalStyle('.threads.style-grid .thread .thread-buttons button { float: right; }')

//This is cross coding because "Title fixer" makes default stuff not compative with this code
addGlobalStyle('.style-grid .custom-link { margin: 0 0 0 0 !important; }')

function fixButtons(post) {
    if (post.getElementsByClassName("post-deleted")[0]) {
        return
    }
  
    let postId = post.getAttribute('data-id')
    let threadId = post.parentNode.getAttribute('data-id')
    let buttonList = []
    let buttonDiv
    
    if (document.querySelector(".threads").classList.contains('style-new_replies')) {

        buttonDiv = post.querySelector(".post-meta .right")

        if (buttonDiv.querySelector(".icon-menu") === null) {
            return
        }

        let voteButtons = buttonDiv.querySelector(".vote-buttons")
    
        buttonList.push(["Post.reply", "icon-reply", postId])
        buttonList.push(voteButtons)

        if (post.classList.contains('op-post')) {
            let follow = document.querySelector("[data-action='Thread.follow'][data-id='" + threadId + "']")
            follow.classList.remove("square-button")
            follow.classList.add("post-button")
            buttonList.push(follow)
        }

        //reverse imagesearch
        if (post.querySelector(".post-file")) {
            const imgUrl = post.querySelector('.post-file').querySelector('a').href
            buttonList.push(["Post.reverseImagesearch", "icon-magnifier", imgUrl])
        }
  
        if (post.classList.contains('own')) {
            buttonList.push(["Post.edit", "icon-pencil-line", postId])
            if(post.classList.contains('op-post')) {
                buttonList.push(["Thread.delete", "icon-trash2", threadId])
            }else {
                buttonList.push(["Post.delete", "icon-trash2", postId])
            }
        }else {
            buttonList.push(["Post.donateGold", "icon-medal-empty", postId])
      
            if(post.classList.contains('op-post')) {
                buttonList.push(["Thread.hide", "icon-minus", threadId])
            }else {
                buttonList.push(["Thread.hideUser", "icon-minus", postId])
            }
        
            buttonList.push(["Post.report", "icon-flag2", postId])
        }
  
        buttonList.forEach((element) => {
            let newButton
            if (Array.isArray(element)) {
        
                newButton = document.createElement("button")
                newButton.classList.add("post-button");
                newButton.setAttribute("data-action", element[0])
                newButton.setAttribute("data-id", element[2])
                newButton.classList.add(element[1]);
            }else {
                newButton = element
            }
            buttonDiv.append(newButton)
        })
      console.log(threadId)
    }else { //Catalog

        buttonDiv = post.parentNode.querySelector(".thread-buttons")
        
        if (buttonDiv.querySelector(".icon-menu") === null) {
            return
        }
      
        if (post.parentNode.classList.contains('own')) {
            buttonList.push(["Thread.delete", "icon-trash2", threadId])
        }else {
            buttonList.push(["Post.report", "icon-flag2", postId])
            buttonList.push(["Thread.hide", "icon-minus", threadId])
        }
    }
  
    buttonDiv.innerHTML = ""
    buttonList.forEach((element) => {
        let newButton
        if (Array.isArray(element)) {
            newButton = document.createElement("button")
            newButton.classList.add("post-button");
            newButton.setAttribute("data-action", element[0])
            newButton.setAttribute("data-id", element[2])
            newButton.classList.add(element[1]);
        }else {
            newButton = element
        }
      
        buttonDiv.append(newButton)
    })
}

/* Create logic for reverse image search */
function clickHandler(e) {
console.log(e.target.dataset.action)
    if (e.target.dataset.action !== 'Post.reverseImagesearch') {
      return //Let everything else work normally
    }
  
    e.stopPropagation(); //Prevent event to start original handler
    e.preventDefault();
  
    const url = "https://www.google.com/searchbyimage?&image_url=" + e.target.dataset.id
    window.open(url, '_blank').focus();
}

var observer = new MutationObserver(mutations => {
    mutations.forEach(({ target }) => {
        const postList = Array.from(target.querySelectorAll(".post"))
        postList.forEach((post) => {
            fixButtons(post)
        })
    })
})

document.querySelectorAll(".post").forEach((post) => {
    fixButtons(post)
})

let threads = ""
if (document.querySelector("html").classList.contains("page-thread")) {
   threads = document.querySelector(".page-thread .thread-replies")
}else {
    threads = document.querySelector(".page-board .threads")
}
observer.observe(threads, { childList: true })

document.querySelector('.threads').addEventListener('click', clickHandler);
