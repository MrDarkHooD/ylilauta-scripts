// ==UserScript==
// @name     Title fixer - ylilauta.org
// @match    *://ylilauta.org/*
// @version  1.14
// @grant    none
// @author   MrDarkHooD
// ==/UserScript==

function addGlobalStyle(css) {
    const nonce = document.getElementsByTagName("script")[0].nonce;
    const head = document.getElementsByTagName('head')[0];
    const style = document.createElement('style');
    style.type = 'text/css';
    style.nonce = nonce;
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle('.style-grid .thread { height: 270px !important; }')
addGlobalStyle('.style-grid .message { height: 100%; overflow: hidden; pointer-events: all !important; }')
addGlobalStyle('.style-grid .customTopic h3  { display: block ruby; margin: 0; overflow: hidden; text-overflow: ellipsis; }')
addGlobalStyle('.style-grid .post { overflow: clip !important; }');
addGlobalStyle('.style-grid .custom-link { margin: 0 0 8px 0; }')
addGlobalStyle('.style-grid .message a { color: var(--link-in-post-color); }')
addGlobalStyle('.style-grid .message a:hover { color: var(--link-in-post-hover-color); }')

addGlobalStyle('.style-new_replies .post.op { margin: 0; }')
addGlobalStyle('.style-new_replies .customTopic h3  { overflow: hidden; text-overflow: ellipsis; margin: 0; }')

addGlobalStyle('.customTopic { padding: 4px 8px; white-space: nowrap; background-color: var(--thread-title-bg-color); }')
addGlobalStyle('.customTopic .icons { display: contents !important; margin-right: 8px !important; }')

function titleFixer(thread) {
  
    // Bubblegum to prevent script making titles multiple times (and failing while doing it)
    if(thread.classList.contains("titleted")) {
      return
    }else {
        thread.classList.add("titleted")
    }
  
    const threadDisplay = document.querySelector(".threads")
  
    
    if (threadDisplay.classList.contains('style-grid') && thread.querySelector(".post-deleted")) {
        return
    }
    
    
    const threadURL = thread.getAttribute("data-id") //change to data-url for more accuracy and less speed
    
    // Create topic string
    let topic = thread.querySelector("h3")
    let customTopic = ""

    if (topic) {
        customTopic = topic["innerHTML"]
        topic.outerHTML = ""
    } else {
        if (threadDisplay.classList.contains('style-grid')) {
            customTopic = thread.querySelector(".message")["innerHTML"]
        } else {
            customTopic = thread.querySelector(".post-message")["innerHTML"]
        }
        customTopic = customTopic.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 100)
    }

    let customLink = document.createElement("a")
        customLink.setAttribute("href", threadURL)
        customLink.classList.add("custom-link");
  
    if (threadDisplay.classList.contains('style-grid')) {
        // Move lock and sticky icons to topic
        let icons = thread.querySelector(".icons")
        if (icons) {
            customTopic = icons["outerHTML"] + customTopic
            icons.outerHTML = ""
        }

        // Get thread link src and get rid of original link
        let link = thread.getElementsByTagName("a")
        if (link[0]) {
            link[0].outerHTML = link[0].innerHTML
        }
      
        // Make image to link
        let threadImage = thread.querySelectorAll("img")[1]
        if (threadImage) {
            
            let postFile = thread.querySelector(".post-file")
            postFile.innerHTML = ""
          
            // Insert post-file div inside of newly made link
            postFile.parentNode.insertBefore(customLink, postFile);
            customLink.appendChild(postFile);

            let newImage = document.createElement("img")
            newImage.setAttribute("src", threadImage["src"])
            newImage.setAttribute("srcset", threadImage["srcset"])
            newImage.setAttribute("alt", "")
            newImage.setAttribute("loading", "lazy")
      
            postFile.append(newImage)
        }
      
        // Make links to be links in catalog
        let message = thread.querySelector(".message")
        const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig
        message.innerHTML = message.innerHTML.replace(urlRegex, "<a href='$1' target='_blank' rel='noopener ugc'>$1</a>")
    }
  
    thread.insertBefore(customLink.cloneNode(true), thread.firstChild);
    thread.firstChild.innerHTML = "<div class='customTopic'><h3>" + customTopic + "</h3></div>"

}

document.querySelectorAll(".thread").forEach((thread) => {
    titleFixer(thread)
});

var observer = new MutationObserver(mutations => {
    mutations.forEach(({ target }) => {
      target.querySelectorAll(".thread").forEach((thread) => {
            titleFixer(thread)
        })
    })
})

if (document.querySelector("html").classList.contains("page-board")) {
    const threads = document.querySelector(".page-board .threads")
    observer.observe(threads, { childList: true })
}
