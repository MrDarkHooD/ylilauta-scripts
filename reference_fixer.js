// ==UserScript==
// @name     Message reference fixer - ylilauta.org
// @match    *://ylilauta.org/*
// @version  1.6
// @grant    none
// @author   MrDarkHooD
// ==/UserScript==

function refLinkFixer(ref) {
    ref.innerHTML = ref.innerHTML.replace(/≫/g, ">>")
    if (!/\d+/.test(ref.innerHTML)) {
        ref.innerHTML = ">>" + ref.getAttribute("data-id") + " (OP)"
    }
}

document.querySelectorAll(".ref").forEach(ref => {
    refLinkFixer(ref)
});

var observer = new MutationObserver(mutations => {
    mutations.forEach(({ target }) => {
        target.querySelectorAll(".ref").forEach((ref) => {
            refLinkFixer(ref)
        })
    })
})

/* Fix reference insertion in message textarea */
function clickHandler(e) {

    if (e.target.dataset.action !== 'Post.reply' && e.target.className !== 'ref') {
      return //Let everything else work normally
    }

    e.stopPropagation(); //Prevent event to start original handler

    /* Jumping */
    if(e.target.className === "ref") {
        const selectedId = e.target.dataset.id
        selectedMessage = document.getElementById(selectedId)
        if(selectedMessage[0]) {
            e.preventDefault(); //Prevent reference links to work as normal links
        }
        window.location.href = "#" + selectedId
        return
    }
  
    e.preventDefault(); //Prevent reference links to work as normal links
  
    /* Post form */
    let postForm = document.getElementsByClassName("post-form")[0]
    let postField = postForm.getElementsByTagName("textarea")[0]
    let referenceNumber = e.target.dataset.id
    let startPos = postField.selectionStart
    let endPos = postField.selectionEnd
    let referenceToInsert = ""

    if (postField.value == "" || postField.value[postField.selectionStart-1] == "\n") { //cursor is in beginning of line
        referenceToInsert = ">>" + referenceNumber + "\n"
    } else if (startPos != postField.value.length && postField.value[postField.selectionStart-1] != "\n") { //cursor is not in beginning or end of line
        referenceToInsert = ">>" + referenceNumber
    } else { //cursor is in end of line
        referenceToInsert = "\n>>" + referenceNumber + "\n"
    }

    postField.value = postField.value.substring(0, startPos)
      + referenceToInsert
      + postField.value.substring(endPos, postField.value.length)

    //Focus cursor after inserted refrence
    postField.focus()
    postField.selectionEnd = startPos + referenceToInsert.length

    //Move post form under refrenced message
}

if(document.querySelector(".threads").classList.contains('style-new_replies')) {
    const threads = document.querySelector(".page-thread .thread-replies")
    observer.observe(threads, { childList: true })
    document.querySelector('.threads').addEventListener('click', clickHandler);
}

